﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;

namespace csharp_practice
{
    class Program
    {
        static void Main(string[] args)
        {

            string m_strFilePath = "http://www.scorespro.com/rss2/live-soccer.xml";
            XmlDocument myXmlDocument = new XmlDocument();
            myXmlDocument.Load(m_strFilePath);

            var m_strFilePath2 = "http://www.scorespro.com/rss2/live-soccer.xml";
            string xmlStr;
            using (var wc = new WebClient())
            {
                xmlStr = wc.DownloadString(m_strFilePath2);
            }
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlStr);

            #region
            //DemonstrateDemo ob = new DemonstrateDemo();
            //ob.TestAdd();



            //string dtTime = GetLocalDateTime(new DateTime(2014, 06, 12, 17, 0, 0));

            //int result = DateTime.Compare(DateTime.Now, Convert.ToDateTime(dtTime));
            //string relationship;

            //if (result < 0)
            //    relationship = "earlier";
            //else if (result == 0)
            //    relationship = "same time ";
            //else
            //    relationship = "later";

            //Console.WriteLine("{0}", relationship);



            //Console.WriteLine(dtTime);
            //Console.WriteLine(DateTime.Now);
            //Console.ReadKey();
            #endregion
        }

        static string GetLocalDateTime(DateTime targetDateTime)
        {
            int fromTimezone = -3;

            int localTimezone;

            if (TimeZoneInfo.Local.BaseUtcOffset.Minutes != 0)
            {
                localTimezone = Convert.ToInt16(TimeZoneInfo.Local.BaseUtcOffset.Hours.ToString() + (TimeZoneInfo.Local.BaseUtcOffset.Minutes / 60).ToString());
            }
            else
            {
                localTimezone = TimeZoneInfo.Local.BaseUtcOffset.Hours;
            }
            DateTime Sdt = targetDateTime;
            DateTime UTCDateTime = targetDateTime.AddHours(-(fromTimezone));
            DateTime localDateTime = UTCDateTime.AddHours(+(localTimezone));
            return localDateTime.ToLongDateString() + " " + localDateTime.ToShortTimeString();
        }

    }

    public class DemonstrateDemo : DemoTest
    {
        public string Add(int n)
        {
            int m = base.Add();
            return "in Child , from Base" + m;
        }

        public void TestAdd()
        {
            Add(1);
            Add();
        }
    }


}
